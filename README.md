# nodercms-build-deploy

用于nodercms编译和打包

## 安装node包
```bash
$ npm i
```

## 编译
```bash
# 开发环境
$ gulp default
# 生产环境
$ gulp build
```

## 编译并打包
```bash
$ gulp build && node build/deploy.js prod
```

### 说明文档
[https://www.bowenhuang.com/js/nodercms-gulp-build-deploy](https://www.bowenhuang.com/js/nodercms-gulp-build-deploy)
