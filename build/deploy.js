const shell = require('shelljs');
const path = require('path');
const pkgInfo = require('../package.json');

(function() {
  const appName = pkgInfo.name;
  const version = pkgInfo.version;

  console.log(`deploy app name: ${appName}`);

  // 版本号合法检查
  if (!version) {
    console.log('not ci tag or not params, don\'t pack!!!');
    return;
  }

  // 进行打包
  deploy(`${appName}-${version}`);

  // 调用发布shell
  function deploy(fileName) {
    shell.exec(`sh ../build/deploy.sh ${fileName} ${version}`, { cwd: path.resolve(__dirname, '../tmp/'), async: true });
  }
}());
