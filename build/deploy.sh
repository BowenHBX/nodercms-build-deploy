#!/usr/bin/env bash

PACK_NAME="$1.tgz"

echo ">>> Installing prod dependencies..."
npm i --production
echo "<<<"
echo ""

echo ">>> Packing..."
touch $PACK_NAME;
mkdir logs
cd logs
mkdir access && mkdir database && mkdir errors && mkdir system
cd ../
tar -czf ./$PACK_NAME --exclude=$PACK_NAME .
echo "Package: $PACK_NAME"
echo "<<<"
echo ""

echo ">>> Show Package's Size..."
ls -lh
echo "<<<"
echo ""

echo ">>> copy file"
cd ../
mkdir pack
mv tmp/$PACK_NAME pack/
echo "<<<"
echo ""